from sklearn.preprocessing import MinMaxScaler
import numpy as np
import pandas as pd


class Recommender:
    def __init__(self, data_object, model_object):
        """
        Recommender creating recommendations.
        :param data_object: instance of Data class
        :param model_object: instance of Model class
        """
        self.data = data_object
        self.model = model_object

    def recommend(self, customer_id, num_items=10):
        """
        Create item recommendations for a customer.
        :param customer_id: customer identifier
        :param num_items: number of recommended items provided by the method
        :return: dataframe with stockcode and description of recommended items
        """
        customers_list = np.array(self.data.customers)
        products_list = np.array(self.data.products)
        cust_ind = np.where(customers_list == customer_id)[0][0]  # returns the index row of customer id
        pref_vec = self.model.training_set[cust_ind, :].toarray()  # get the ratings from the training set
        # ratings matrix
        pref_vec = pref_vec.reshape(-1) + 1  # add 1 to everything, so that items not purchased yet become equal to 1
        pref_vec[pref_vec > 1] = 0  # make everything already purchased zero
        rec_vector = self.model.user_vecs[cust_ind, :].dot(self.model.item_vecs.T)  # get dot product of user vector
        # and all item vectors
        # scale this recommendation vector between 0 and 1
        min_max = MinMaxScaler()
        rec_vector_scaled = min_max.fit_transform(rec_vector.reshape(-1, 1))[:, 0]
        recommend_vector = pref_vec * rec_vector_scaled
        # items already purchased have their recommendation multiplied by zero
        product_idx = np.argsort(recommend_vector)[::-1][:num_items]  # sort the indices of the items into order
        # of best recommendations
        rec_list = []  # start empty list to store items
        for index in product_idx:
            code = products_list[index]
            rec_list.append([code, self.data.item_lookup.description.loc[self.data.item_lookup.stockcode == code].
                            iloc[0]])
            # append descriptions to the list
        codes = [item[0] for item in rec_list]
        descriptions = [item[1] for item in rec_list]
        recommendation = pd.DataFrame({'StockCode': codes, 'Description': descriptions})
        return recommendation[['StockCode', 'Description']]  # switch order of columns around
