import numpy as np


class Customer:
    def __init__(self, customer_id, data_object, model_object):
        """
        Customer - purchaser.
        :param customer_id: id of a customer
        :param data_object: instance of Data class
        :param model_object: instance of Model class
        """
        self.customer_id = customer_id
        self.data = data_object
        self.model = model_object
        self.items_purchased = None

    def get_items_purchased(self):
        """
        Retrieve items that have been already purchased by a specific user in the training set.
        :return: items with their description purchased by a given customer
        """
        customers_list = np.array(self.data.customers)
        products_list = np.array(self.data.products)
        item_lookup = self.data.create_item_lookup()
        cust_ind = np.where(customers_list == self.customer_id)[0][0]  # returns the index row of our customer id
        purchased_ind = self.model.training_set[cust_ind, :].nonzero()[1]  # get column indices of purchased items
        prod_codes = products_list[purchased_ind]  # get the stock codes for our purchased items
        self.items_purchased = item_lookup.loc[self.data.item_lookup.stockcode.isin(prod_codes)]
        return self.items_purchased
