import sys
import os
import logging
from logging.handlers import TimedRotatingFileHandler
from pythonjsonlogger import jsonlogger
import datetime
from pytz import timezone

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from logging_service.file_logger import Logger
from conf import log_config as cfg


localtz = timezone('Europe/Bratislava')

supported_keys = [
    'filename',
    'funcName',
    'levelname',
    'message',
    'emailto',
    'subject'
]


class JsonLogger(Logger):
    def __init__(self):
        Logger.__init__(self)
        if not os.path.exists('/app/logs'):
            os.makedirs('/app/logs')
        log_file_name = cfg.log_fileName
        log_format = lambda x: ['%({0:s})'.format(i) for i in x]
        custom_format = ' '.join(log_format(supported_keys))

        formatter = jsonlogger.JsonFormatter(custom_format)

        # # used with flask+uwsgi
        # log_handler_std = logging.StreamHandler()
        # log_handler_std.setFormatter(formatter)
        # logger.addFilter(SystemLogFilter())
        # logger.addHandler(log_handler_std)

        # without flask+wsgi
        log_handler = TimedRotatingFileHandler(filename=log_file_name,
                                               when="d",
                                               interval=1,
                                               backupCount=30)
        log_handler.setLevel(cfg.log_level)
        log_handler.setFormatter(formatter)
        self.logger.addFilter(SystemLogFilter())
        self.logger.addHandler(log_handler)


class SystemLogFilter(logging.Filter):
    @staticmethod
    def filter(record):
        record.datetime = localtz.localize(datetime.datetime.now()).strftime("%Y-%m-%dT%H:%M:%S.%f%z")
        return True
