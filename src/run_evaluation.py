from argparse import ArgumentParser
import scipy.sparse as sparse
import seaborn as sn

import data_processing.process_data as proc_data
import model.ml_model as mlm
import model.score_model as score
# from db_communicator import db_connector as dbc
from logging_service import logger as log


def model_evaluation(mode, connection=None):
    """
    Evaluating current ML model.
    :param mode: "local_test" or "test" or "deploy"
    :param connection: psycopg2 connection object
    :return:
    """
    # load and preprocess data
    log.i("Running data processing.")
    # TODO: evaluation based on csv data extract, todo = make it based on actual data in the DB
    #  DB must be made accessible for runner
    if not connection:
        data = proc_data.Data(mode,
                              # csv="D:\\DevProjects\\wolflings_recommender_engine\\src\\data\\or_dummy.csv")
                              csv="/app/data/or_dummy.csv")
    else:
        data = proc_data.Data(mode,
                              connection)
    data.load_data_csv()
    data.clean_data()
    data.prepare_data()
    data.create_item_lookup()

    # call ml_model pipeline
    log.i("Running ML model.")
    ml_model = mlm.Model(data.ratings_sparse)
    product_train, product_test, product_users_altered = ml_model.make_train(pct_test=0.2)
    ml_model.als_model()

    # evaluate model - # AUC for our recommender system
    (model_score, benchmark_score_pop, benchmark_score_random, confussion_matrix) = score.calc_mean_auc(
                                                                     product_train,
                                                                     product_users_altered,
                                                                     [sparse.csr_matrix(ml_model.user_vecs),
                                                                      sparse.csr_matrix(ml_model.item_vecs.T)],
                                                                     product_test)

    # print((benchmark_score_random, benchmark_score_pop, model_score))
    # print(confussion_matrix)

    # store scores to text file
    with open("model_score.txt", "w") as text_file:
        text_file.write("- Random model score: %s \n"
                        "- Most-popular-items model score: %s \n"
                        "- Our ML model score: %s" % (benchmark_score_random, benchmark_score_pop, model_score))

    # plot confussion matrix and store to image file
    sn.set(font_scale=1.4)  # for label size
    sn_plot = sn.heatmap(confussion_matrix, annot=True, annot_kws={"size": 16}).get_figure()
    sn_plot.savefig('confusion_matrix.png')


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-a')  # mode switch: mode = args.a
    args = parser.parse_args()

    log.i("Running model evaluation.")
    # connection = dbc.get_connection(args.a)
    model_evaluation(args.a)
    # connection.close()
    log.i("Model evaluation finished.")
