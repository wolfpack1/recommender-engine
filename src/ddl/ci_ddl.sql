DROP TABLE IF EXISTS wolflings_online_retail_dummy;
CREATE TABLE wolflings_online_retail_dummy
(
	InvoiceNo character varying(20),
	StockCode character varying(20),
	Description character varying(255),
	Quantity integer,
	InvoiceDate timestamp,
	UnitPrice float,
	CustomerID integer,
	Country character varying(50)
);