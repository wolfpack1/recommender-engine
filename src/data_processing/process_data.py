import os
import sys
import pandas as pd
import pandas.io.sql as sqlio
from pandas.api.types import CategoricalDtype
import scipy.sparse as sparse
import numpy as np

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from conf import config as cfg


class Data:
    def __init__(self, mode, connection=None, data=None, table=None, csv=None):
        """
        Load, clean and prepare data to be used by recommendations engine
        for making recommendations. Only one of the parameters (data, table, csv)
        can be provided.
        :param mode: either local_test or test (ci pipeline) or deploy
        :param connection: psycopg2 DB connection object
        :param data: if provided use this ad hoc data; the data must be
            pandas dataframe
        :param table: if provided use this ad hoc data
        :param csv: route for csv with data, if provided use this ad hoc data
        """
        count = 0
        for i in (data, table, csv):
            if i:
                count += 1
        if count > 1:
            raise Exception("Only one of the parameters (data, table, csv) can be provided.")

        # connection must be provided if working with rds
        if (table and not data and not csv) or (not table and not data and not csv):
            assert connection

        self.mode = mode
        if connection:
            self.connection = connection
        if data:
            assert isinstance(data, pd.DataFrame)
            self.data = data
        else:
            self.data = None
        self.csv = csv
        self.table = table
        self.data_light = None
        self.customers = []
        self.products = []
        self.quantities = []
        self.ratings_sparse = None
        self.item_lookup = None

    def __str__(self):
        return str(self.data.head())

    def load_data_rdb(self):
        """
        Load transactions data from relational database provided in config.
        :return: data loaded as pandas dataframe
        """
        if self.table:
            table = self.table
        else:
            table = cfg.database_details[self.mode]['table']
        self.data = sqlio.read_sql_query("select * from %s" % table,
                                         self.connection)

        return self.data

    def load_data_csv(self):
        """
        Load transactions data from csv file.
        :return: data loaded as pandas dataframe
        """
        if not self.csv:
            raise Exception("load_data_csv method cannot be called for this object.")
        self.data = pd.read_csv(self.csv, delimiter=',', header=0, names=["invoiceno", "stockcode", "description",
                                                                          "quantity", "invoicedate", "unitprice",
                                                                          "customerid", "country"])

        return self.data

    def clean_data(self):
        """
        Clean data. For now, primitive cleaning - record must bear all
        necessary info, i.e. customerid, stockcode, quantity.
        """
        print(self.data.columns)
        self.data = self.data.loc[pd.isnull(self.data.customerid) == False]
        self.data = self.data.loc[pd.isnull(self.data.stockcode) == False]
        self.data = self.data.loc[pd.isnull(self.data.quantity) == False]

        # return self.data.info()

    def prepare_data(self):
        """
        Prepare data for recommeder model.
        :return: sparsity of customer-item matrix
        """
        self.data['customerid'] = self.data.customerid.astype(int)
        self.data_light = self.data[['stockcode', 'quantity', 'customerid']]
        self.data_light = self.data_light.groupby(['customerid', 'stockcode']).sum().reset_index()
        # replace a sum of zero purchases with a one to indicate purchased
        self.data_light.loc[self.data_light.quantity == 0, 'quantity'] = 1
        # only get customers where purchase totals were positive
        self.data_light = self.data_light.query('quantity > 0')

        # create sparse matrix
        self.customers = list(np.sort(self.data_light.customerid.unique()))  # unique customers
        self.products = list(self.data_light.stockcode.unique())  # unique products that were purchased
        self.quantities = list(self.data_light.quantity)  # quantities

        # get the associated row indices
        rows = self.data_light.customerid.astype(CategoricalDtype(categories=self.customers)).cat.codes
        # get the associated column indices
        cols = self.data_light.stockcode.astype(CategoricalDtype(categories=self.products)).cat.codes
        self.ratings_sparse = sparse.csr_matrix((self.quantities, (rows, cols)), shape=(len(self.customers),
                                                                                        len(self.products)))

        return self.calculate_sparsity(self.ratings_sparse)

    def create_item_lookup(self):
        """
        Create lookup linking stockcode and item description.
        """
        self.item_lookup = self.data[['stockcode', 'description']].drop_duplicates()
        self.item_lookup['stockcode'] = self.item_lookup.stockcode.astype(str)
        return self.item_lookup

    @staticmethod
    def calculate_sparsity(sparse_matrix):
        """
        Calculate sparsity of a matrix.
        :param sparse_matrix: matrix for which sparsity is to be calculated
        :return: sparsity of sparse_matrix
        """
        matrix_size = sparse_matrix.shape[0] * sparse_matrix.shape[1]  # number of possible interactions in the matrix
        num_purchases = len(sparse_matrix.nonzero()[0])  # number of items interacted with
        sparsity = 100 * (1 - (num_purchases / matrix_size))
        return sparsity


