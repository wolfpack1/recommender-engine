import os
import sys
import pandas as pd

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from db_communicator.db_handler import run_query
from logging_service import logger as log


def recommend_all_customers(recommender, customer_list, num_r):
    """
    Create recommendations for listed customers.
    :param recommender: instance of Recommender class
    :param customer_list: list of customers for which to make recommendations
    :param num_r: number of recommendations per customer
    :return: dataframe with recommendations for listed customers
    """
    recommnedations = pd.DataFrame([])
    for customer in customer_list:
        recommendation = recommender.recommend(customer, num_items=num_r)
        recommendation["CustomerId"] = pd.Series([customer] * len(recommendation.index))
        recommendation["Priority"] = recommendation.index + 1
        recommnedations = pd.concat([recommnedations, recommendation], axis=0)
    return recommnedations


def clear_output_table(connection):
    """
    Clear Recommendations table.
    :param connection: psycopg
    """
    log.i("Cleaning DB.")
    query = """DROP TABLE IF EXISTS wolflings_recommendations;
               CREATE TABLE wolflings_recommendations
               (
                StockCode character varying(20),
                Description character varying(255),
                CustomerId integer,
                Priority integer
                );"""
    run_query(connection, query)


def populate_recommendations_table(connection, df):
    """
    Populate Recommendations table with new recommendations - output df dataframe to it.
    df columns are (Stockcode, Description, CustomerId, Priority).
    """
    log.i("Populating DB table with recommendations.")
    for i, row in df.iterrows():
        query = "INSERT INTO wolflings_recommendations (Stockcode, Description, CustomerId, Priority)" \
                "VALUES ('{}', '{}', {}, {})".format(row['StockCode'],
                                                     row['Description'].replace("'", "''"),
                                                     row['CustomerId'],
                                                     row['Priority'])
        run_query(connection, query)
    log.i("DB table populated")
