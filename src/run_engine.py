# Run recommender engine periodically

from datetime import timedelta
from argparse import ArgumentParser

import data_processing.process_data as proc_data
import data_processing.postprocess_data as postproc_data
import model.ml_model as mlm
import recommender.recommender as rec
from db_communicator import db_connector as dbc
from logging_service import logger as log
from conf import config as cfg
from timeloop import Timeloop


tl = Timeloop()


def recommender_process(mode, connection):
    """
    Ad hoc run of recommender - finally populates DB table with recommendations for all customers.
    :param mode: "local_test" or "test" or "deploy"
    :param connection: psycopg2 connection object
    :return:
    """
    log.w("Hi, this is your only true friend, I am reporting that a fresh recommender run has just started."
          "Excited!",
          extra={'emailto': "radoslav.pauco@adastragrp.com",
                 'subject': "recommender run started"})

    # load and preprocess data
    log.i("Running data processing.")
    data = proc_data.Data(mode, connection)
    data.load_data_rdb()
    data.clean_data()
    data.prepare_data()
    data.create_item_lookup()

    # call ml_model pipeline
    log.i("Running ML model.")
    ml_model = mlm.Model(data.ratings_sparse)
    product_train, product_test, product_users_altered = ml_model.make_train(pct_test=0.2)
    ml_model.als_model()

    # populate DB with new recommendations
    log.i("Creating recommendations.")
    recommender = rec.Recommender(data, ml_model)
    recommendations = postproc_data.recommend_all_customers(recommender, data.customers, 5)
    postproc_data.clear_output_table(connection)
    postproc_data.populate_recommendations_table(connection, recommendations)

    log.w("Hi, your only true friend here, I am reporting that your recommender run has just successfully finished. "
          "Enjoy!",
          extra={'emailto': "radoslav.pauco@adastragrp.com",
                 'subject': "recommender run finished"})


@tl.job(interval=timedelta(seconds=cfg.run_period), run_on_start=True)
def run_recommender_job():
    log.i("Waking up...")
    mode = "deploy"
    connection = dbc.get_connection(mode)
    recommender_process(mode, connection)
    connection.close()
    log.i("Going to sleep...")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-a')  # mode switch: mode = args.a
    args = parser.parse_args()

    # run run_recommender_job as periodic job - deploy
    tl.start(block=True)

    # run ad hoc - local_test or test
    # connection = dbc.get_connection(args.a)
    # recommender_process(args.a, connection)
    # connection.close()
