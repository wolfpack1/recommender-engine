from sklearn import metrics
import numpy as np
import random


def auc_score(predictions, test):
    """
    Calculate the area under the curve (AUC) using sklearn's metrics.
    :param predictions: prediction output
    :param test: the actual target result you are comparing to
    :return: AUC - area under the Receiver Operating Characteristic curve)
    """
    fpr, tpr, thresholds = metrics.roc_curve(test, predictions)
    return fpr, tpr, metrics.auc(fpr, tpr)


def calc_mean_auc(training_set, altered_users, predictions, test_set):
    """
    Calculate the mean AUC by user for any user that had their user-item matrix altered.
    :param training_set: the training set resulting from make_train, where a certain percentage of the original
        user/item interactions are reset to zero to hide them from the model
    :param predictions: The matrix of your predicted ratings for each user/item pair as output from the implicit MF.
        These should be stored in a list, with user vectors as item zero and item vectors as item one.
    :param altered_users: the indices of the users where at least one user/item pair was altered from make_train
        function
    :param test_set: the test set constructed earlier from make_train function
    :return: The mean AUC (area under the Receiver Operator Characteristic curve) of the ML model,
        the most-popular-items model, and random model as benchmarks. The last returned value is "mean"
        confusion matrix.
    """
    model_auc = []  # an empty list to store the AUC for each user that had an item removed from the training set
    popularity_auc = []  # to store AUC scores of popular-items model
    random_auc = []  # to store AUC scores of random-items model
    conf_matrices = []  # to store confussion matrices

    pop_items = np.array(test_set.sum(axis=0)).reshape(-1)  # get sum of item interactions to find most popular
    random_items = np.copy(pop_items)
    random.shuffle(random_items)  # reshuffle
    item_vecs = predictions[1]

    for user in altered_users:  # iterate through each user that had an item altered
        training_row = training_set[user, :].toarray().reshape(-1)  # get the training set row
        zero_inds = np.where(training_row == 0)  # find where the interaction had not yet occurred

        # get the predicted values based on our user/item vectors
        user_vec = predictions[0][user, :]
        pred = user_vec.dot(item_vecs).toarray()[0, zero_inds].reshape(-1)  # get only the items that were originally 0

        # select all ratings from the MF prediction for this user that originally had no interaction
        actual = test_set[user, :].toarray()[0, zero_inds].reshape(-1)
        # select the binarized yes/no interaction pairs from the original full data
        # that align with the same pairs in training

        pop = pop_items[zero_inds]  # get the item popularity for our chosen items
        randomized_pop = random_items[zero_inds]

        fpr, tpr, auc_score_val = auc_score(pred, actual)
        model_auc.append(auc_score_val)  # calculate AUC for the given user and store

        fpr, tpr, auc_score_val = auc_score(pop, actual)
        popularity_auc.append(auc_score_val)  # calculate AUC using most popular and store

        fpr, tpr, auc_score_val = auc_score(randomized_pop, actual)
        random_auc.append(auc_score_val)  # calculate AUC using most popular and store

        pred[np.where(pred < 0.8)] = 0  # approximation to get binary matrix
        pred[np.where(pred >= 0.8)] = 1  # approximation to get binary matrix
        conf_matrices.append(metrics.confusion_matrix(actual, pred, labels=[0, 1]))

    return float('%.3f' % np.mean(model_auc)), \
           float('%.3f' % np.mean(popularity_auc)), \
           float('%.3f' % np.mean(random_auc)), \
           sum(conf_matrices)/len(conf_matrices)
