import random
import numpy as np
import implicit


class Model:
    def __init__(self, ratings):
        """
        Our ML model for recommendations.
        :param ratings: user-item "ratings" matrix
        """
        self.ratings = ratings
        self.training_set = None
        self.test_set = None
        self.implicit_model = None
        self.masked_user_rows = []
        self.user_vecs = None
        self.item_vecs = None

    def make_train(self, pct_test=0.2):
        """
        This function will take in the original user-item matrix and "mask" a percentage of the original ratings where
        a user-item interaction has taken place for use as a test set. The test set will contain all of the original
        ratings, while the training set replaces the specified percentage of them with a zero in the original ratings
        matrix.
        :param pct_test: the percentage of user-item interactions where an interaction took place that you want to mask
        in the training set for later comparison to the test set, which contains all of the original ratings.
        :return:
        training_set - The altered version of the original data with a certain percentage of the user-item pairs
        that originally had interaction set back to zero.
        test_set - A copy of the original ratings matrix, unaltered, so it can be used to see how the rank order
        compares with the actual interactions.
        masked_user_rows - From the randomly selected user-item indices, which user rows were altered in the training data.
        This will be necessary later when evaluating the performance via AUC.
        """
        self.test_set = self.ratings.copy()
        self.test_set[self.test_set != 0] = 1  # store the test set as a binary preference matrix
        self.training_set = self.ratings.copy()
        nonzero_inds = self.training_set.nonzero()  # find the indices in the ratings data where an interaction exists
        nonzero_pairs = list(
            zip(nonzero_inds[0], nonzero_inds[1]))  # zip these pairs together of user, item index into list
        random.seed(0)  # set the random seed to 0 for reproducibility
        num_samples = int(
            np.ceil(pct_test * len(nonzero_pairs)))  # round the number of samples needed to the nearest integer
        samples = random.sample(nonzero_pairs,
                                num_samples)  # sample a random number of user-item pairs without replacement
        user_inds = [index[0] for index in samples]  # get the user row indices
        item_inds = [index[1] for index in samples]  # get the item column indices
        self.training_set[user_inds, item_inds] = 0  # assign all of the randomly chosen user-item pairs to zero
        self.training_set.eliminate_zeros()  # get rid of zeros in sparse array storage after update to save space
        self.masked_user_rows = list(set(user_inds))
        return self.training_set, self.test_set, self.masked_user_rows

    def als_model(self, alpha=15, factors=20, regularization=0.1, iterations=50):
        """
        Alternating least square model. Reference: http://yifanhu.net/PUB/cf.pdf (Yifan Hu, Koren, Volinsky 2008).
        :param alpha: The parameter associated with the confidence matrix discussed in the paper, where
            Cui = 1 + alpha*Rui. The paper found a default of 40 most effective. Decreasing this will decrease
            the variability in confidence between various ratings.
        :param factors: number of latent features
        :param regularization: used for regularization during alternating least squares. Increasing this value may
            increase bias but decrease variance.
        :param iterations: number of algos iterations
        :return: user_vecs - array of latent factors for each user in the training set
                 item_vecs - array of latent factors for each item in the training set
        """
        # TODO: these parameters came from previous experiments with the data; feature to implement -
        #  methods for finding good parameters based on cross validation and grid search
        self.implicit_model = implicit.als.AlternatingLeastSquares(factors=factors,
                                                                   regularization=regularization,
                                                                   iterations=iterations)
        self.implicit_model.fit((self.training_set.T * alpha).astype('double'))

        self.user_vecs = self.implicit_model.user_factors
        self.item_vecs = self.implicit_model.item_factors
        return self.user_vecs, self.item_vecs
