import os
import sys
import pytest
import psycopg2

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from conf import config as cfg


def pytest_addoption(parser):
    parser.addoption("--mode", action="store", default="test")


@pytest.fixture(scope='session')
def mode(request):
    """pytest mode - local_test or test"""
    mode_value = request.config.option.mode
    if mode_value is None:
        mode_value = "test"
    return mode_value


@pytest.fixture
def connection(mode):
    """psycopg2 connection object - connections to transactions db"""
    connection = psycopg2.connect(
        dbname=cfg.database_details.get(mode).get("database"),
        user=cfg.database_details.get(mode).get("user"),
        password=cfg.database_details.get(mode).get("password"),
        host=cfg.database_details.get(mode).get("host")
    )
    yield connection
    connection.close()
