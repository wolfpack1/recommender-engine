import os
import sys
import pandas as pd

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

import data_processing.process_data as proc_data
import model.ml_model as mlm
import recommender.customer as cust
import recommender.recommender as rec


class TestRecommender():
    def test_customer_recommender(self, mode, connection):
        # load and preprocess data
        data = proc_data.Data(mode, connection)
        data.load_data_rdb()
        data.clean_data()
        data.prepare_data()

        # call ml_model pipeline
        ml_model = mlm.Model(data.ratings_sparse)
        training_set, test_set, masked_user_rows_lst = ml_model.make_train(pct_test=0.2)
        ml_model.als_model()

        # test customer
        customer = cust.Customer(17850, data, ml_model)
        assert customer.customer_id == 17850
        assert customer.get_items_purchased().shape == (13, 2)

        # test recommender
        recommender = rec.Recommender(data, ml_model)
        recommendations = recommender.recommend(customer.customer_id, num_items=10)
        assert recommendations.shape == (10, 2)
