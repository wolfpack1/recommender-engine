import os
import sys
import pandas as pd

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

import data_processing.process_data as proc_data


class TestData:
    def test_data(self, mode, connection):
        data = proc_data.Data(mode, connection)

        df = data.load_data_rdb()
        assert isinstance(df, pd.DataFrame)

        data.clean_data()
        assert len(data.data) == 998

        data.create_item_lookup()
        assert data.item_lookup.shape == (590, 2)

        assert round(data.prepare_data(), 2) == 96.77
