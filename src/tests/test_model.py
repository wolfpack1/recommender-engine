import os
import sys
from scipy.sparse import csr_matrix
import numpy as np

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

import data_processing.process_data as proc_data
import model.ml_model as mlm
from model.score_model import calc_mean_auc


class TestModel:
    def test_model(self, mode, connection):
        # load and preprocess data
        data = proc_data.Data(mode, connection)
        data.load_data_rdb()
        data.clean_data()
        data.prepare_data()

        # test ml_model
        ml_model = mlm.Model(data.ratings_sparse)

        training_set, test_set, masked_user_rows_lst = ml_model.make_train(pct_test=0.2)
        assert isinstance(training_set, csr_matrix)
        assert isinstance(test_set, csr_matrix)
        assert len(masked_user_rows_lst) == 35

        ml_model.als_model()
        assert isinstance(ml_model.user_vecs, np.ndarray)
        assert isinstance(ml_model.item_vecs, np.ndarray)

        # test score_model
        score = calc_mean_auc(ml_model.training_set,
                              ml_model.masked_user_rows,
                              [csr_matrix(ml_model.user_vecs), csr_matrix(ml_model.item_vecs.T)],
                              ml_model.test_set)
        assert len(score) == 4
        assert score[1] == 0.685
