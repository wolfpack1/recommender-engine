import os
import sys

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

import data_processing.process_data as proc_data
import data_processing.postprocess_data as postproc_data
import model.ml_model as mlm
import recommender.customer as cust
import recommender.recommender as rec


def test_customer_recommender(mode, connection):
    # load and preprocess data
    data = proc_data.Data(mode, connection, table="wolflings_online_retail")
    data.load_data_rdb()
    data.clean_data()
    data.prepare_data()
    data.create_item_lookup()

    # call ml_model pipeline
    ml_model = mlm.Model(data.ratings_sparse)
    training_set, test_set, masked_user_rows_lst = ml_model.make_train(pct_test=0.2)
    ml_model.als_model()

    # customer = cust.Customer(12361, data, ml_model)
    print(cust.get_items_purchased())

    recommender = rec.Recommender(data, ml_model)
    recommendations = recommender.recommend(cust.customer_id, num_items=10)
    print(recommendations)

    recommendations = postproc_data.recommend_all_customers(recommender, data.customers, 5)

    postproc_data.clear_output_table(connection)
    postproc_data.populate_recommendations_table(connection, recommendations)
