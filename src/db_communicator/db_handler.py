# DB Communicator
# Handling DB queries

import os
import sys
import time

my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, my_path + '/../')

from logging_service import logger as log


def run_query(connection, query):
    """
    Run query against DB.
    :param connection: DB connection - psycopg2 connection object
    :param query: query to be run
    :return: query result, row count
    """
    log.i('Running query: ' + query)
    t = time.time()
    row_cnt = 0
    try:
        cur = connection.cursor()
        cur.execute(query)
        connection.commit()
        row_cnt = cur.rowcount
    except Exception as e:
        log.e('Query failed. Query detail: %s .:. Exception detail: %s' % (query, e))
        connection.rollback()
        cur.close()
        raise Exception(e)
    try:
        result_set = cur.fetchall()
        log.i('Query result: ' + str(result_set))
    except Exception as e:
        log.i('Query returned no results.')
        result_set = None
    finally:
        cur.close()
        delta = time.time() - t
        log.i('Query run time seconds: ' + str(delta))
        return result_set, row_cnt
