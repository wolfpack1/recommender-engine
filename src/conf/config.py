# Config file

import os


# engine run period in seconds
run_period = 300


def return_secret_creds(flavor):
    """Return deployment credentials available as environment variables in container."""
    try:
        if flavor == "db":
            password = os.environ['db_password']
            username = os.environ['db_username']
        return (username, password)
    except KeyError:
        #  the credentials above available and needed only in Kubernetes, not in CI/CD pipeline
        return ('', '')
    except Exception as e:
        raise Exception('Config reading failed with: %s' % e)


database_details = {
    'local_test': {
        'database': 'postgres',
        'user': 'postgres',
        'password': 'postgres',
        'host': 'localhost',
        'port': 5432,
        'table': 'wolflings_online_retail_dummy',
        'dummy_csv': 'D:\\DevProjects\\wolflings_recommender_engine\\src\\tests\\or_dummy.csv'
            },
    'test': {
            'database': 'paranoid_mole',
            'user': 'runner',
            'password': '',
            'host': 'postgres',
            'table': 'wolflings_online_retail_dummy',
            'dummy_csv': '/app/tests/or_dummy.csv'
                },
    'deploy': {
            'database': 'd_ocn',
            'user': return_secret_creds("db")[0],
            'password': return_secret_creds("db")[1],
            'host': 'rztvnode452.cz.tmo',
            'table': 'wolflings_online_retail',
            'port': '6432'
                }
    }
