# Logging configuration

import logging


# log_fileName = 'D:\\DevProjects\\wolflings_recommender_engine\\src\\logs\\app_log.log'  # for local testing
log_fileName = '/app/logs/app_log.log'  # in gitlab or kubernetes
# log_env = 'local'  # log into file inside container
log_env = 'fluentbit'  # log to fluentbit pipe -> es

log_level = logging.WARNING
log_size = 10000000  # in bytes
