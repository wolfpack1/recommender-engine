## Recommender engine - MLOps

Demonstration of building of a recommender with 
the MLOps principles.

Recommender is heavily inspired by this great article:
https://jessesw.com/Rec-System/

Continous machine Learning (CML) is used:
- https://cml.dev/
- https://github.com/iterative/cml/wiki/CML-with-GitLab

TLDR: each commit triggers CI step running all unit tests, creating coverage report as well as small report of the perfomance of the ML model. The report is being fed automatically to merge-request page, see figure below. Note that the confussion matrix here is used just to demonstrate that visuals can be used in the report as well. On the other hand, the scores are properly defined and reflect the real performance of the ML model.

![](images/cml_score_small.png)

Note that the CD part is disabled here, but the code reflects working environment elsewhere. Also logging is being setup in a way that all logs are piped through Fluentbit to Elasticsearch.
