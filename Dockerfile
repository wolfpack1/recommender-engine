FROM registry.gitlab.com/wolfpack1/recommender-engine/base-image:latest

COPY src /app

# for run app
CMD ["python", "/app/run_engine.py", "-a", "deploy"]

# dummy start
# CMD tail -f /dev/null

